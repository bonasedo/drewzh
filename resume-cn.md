# 赵颖豪

**文档工程师**

长期从事 ToB 企业文档写作；熟悉中英文技术文档最佳实践；曾深度参与 ToB、ToC 用户界面文案撰写。


<details>
<summary>联系方式</summary>

- [LinkedIn Profile](https://www.linkedin.com/in/drew-zhao-b9162256/)
- <drewzhao@outlook.com>
- <details><summary>Wechat: drewyy</summary><img src="resume-wechat.jpeg" width="256" height="256"></details>

</details>

## 个人评价

- 重视调研、实践：在敏捷开发环境下长期工作，习惯主动收集信息，大量调研，动手实操
- 积极沟通：努力通过工具、流程、宣传、实践提升透明度，消除信息不对称
- 独立自主：独立搭建基于免费工具的文档代码化流程，实现内容重用，产出 PDF 与静态网站；独立设计与实现 PDF 样式
- 结果导向：注重团队协作，协同进步，争取最优结果

## 教育经历

2011/09 – 2014/07 **北京大学**，软件与微电子学院，软件工程（文本翻译）硕士

2007/09 – 2011/07 **湖南农业大学**，外国语学院，英语专业本科

## 工作经历

### 2020/07 – 至今

[北京云中融信网络科技有限公司](https://www.rongcloud.cn/) 文档工程师

**职责**

- 作为新引入的文档工程师角色，接手中文开发者文档重建项目。主要成果包括：统一文档内容规则；强力推行了文档 Review 机制与工作流；新制作了针对产品概念、新手入门的专门文档。
- 接手公司出海业务的文档需求；从翻译供应商获取译文，查漏补缺，进行订正，并负责上线；同时为优化本地化的结果，持续改造中文源文档。
- 延续和建设公司「文档共建」的工作机制，并致力于通过写作范例、Review 意见和面对面沟通提升人员的中文文档写作水平。

**工具**

- Markdown + VS Code

- Git/SourceTree/Gitlab

- OmegaT + Okapi filters（计算机辅助翻译工具）

### 2020/06 – 2021/07

[特斯联科技集团](https://www.tslsmart.com/) 文档工程师

**职责**

- 作为唯一文档工程师，与软硬件研发团队对接，产出中英文版本的开箱手册、产品手册、快速上手指南、安装与运维指南、使用指南等文档（PDF格式）。
- 参与迪拜世博项目交付，涉及调研沟通、场景定义、产品修改、场景测试、团队协作支持等。
- 新建基于 SharePoint 内网平台并推广，解决海内外团队间沟通协作不足、资源难以共享的问题。
- 与安全部门对接，负责迪拜世博会相关机器人产品 Data Privacy Impact Assesment（DPIA）的调研、整理、输出。
- 其他支持性翻译，如机器人客户端字符串，对海外的报告等。

**工具**：

- Markdown + VS Code / DITA / DITA Open Toolkit (编排 Markdown 生成 PDF，实现内容重用）

- Git/SourceTree/Gitlab

- Adobe Illustrator / Adobe Photoshop / Inkscape / Gimp / ImageMagick

- OmegaT + Okapi filters（计算机辅助翻译工具）

- SharePoint

### 2018/10 – 2020/05

[北京志凌海纳科技有限公司（SmartX）深圳分公司](https://www.smartx.com) 文档工程师

**职责**

- 负责 SmartX 超融合平台 SMTX OS 的虚拟化、存储组文档，包括软件安装部署、使用指南（GUI/CLI）、运维手册等。利用结构化写作思想改进内容，提升了可读性；梳理了文档结构，使内容模块化，提高了重用度，并简化了大量重复内容。
- 负责文档线上化工作，设计了从写作到发布的工作流程。实现了从 Google Docs 到 Markdown 的转换；实现了从同一 Markdown 源输出适用于 PDF 与 HTML 的内容；利用开源免费工具，节约了成本；帮助团队另一位同事学习相关工具。
- 负责翻译或审核 SMTX OS 英文图形界面的英文字符串，对界面上所有英文翻译以及 UI 设计提出改进建议。
- 参与翻译市场部门撰写的客户案例；将 SmartX 网站翻译成英文；主导制作 SmartX 官网的第一批技术解决方案，包括场景类方案与行业类解决方案，如“虚拟化和私有云”、“VDI” 等场景或金融、餐饮、房地产、制造等行业。

**工具**：

- Google Docs/Markdown + VS Code

- Git/SourceTree/Gitlab/Github/

- Gitlab CI

- Docusaurus（Facebook 开源的静态网站生成器）

- Inkscape（替代 Adobe Illustrator）/Graphviz

- DITA/DITA Open toolkit。借用 DITA map 来组织多个 markdown 文件生成有结构的文档，实现重用与最低为 topic 级别的 conditionalized output。

- SmartCat（在线计算机辅助翻译工具）

### 2018/08 – 2018/08

[猎豹移动（北京）](https://www.cmcm.com) 本地化项目经理

**项目**

猎豹启动器，猎豹键盘，Bawls of Arrows，Bitrue，猎豹安全大师等。

**职责**

- 撰写或翻译猎豹移动产品界面上的字符串为英文，将英文交付给第三方（创思立信、RWS Moravia、OneHourTranslation）翻译为20+语言；追踪进度；人工对比新旧语言资源文件、并选择所需翻译提交到产品代码中（SVN/Git）。成功地在团队人手不足地情况下支持了产品一周一版的迭代需求。
- 翻译运营宣传文案，内部邮件等。

**工具**

- Git/SVN/SourceTree

- BeyondCompare

- Crowdin（在线众包计算机辅助翻译工具）

### 2014/12 – 2018/7

[华睿泰科技(北京)有限公司（Veritas）](https://www.veritas.com) 文档工程师

**项目**

- NetBackup Appliance (备份与恢复一体机)

- NetBackup Virtual Appliance (备份与恢复一体机虚拟产品)

- Velocity Appliance (CDM复制数据管理)

- Access Appliance (作数据长期保存的一体机)

**职责**

- 开发软件文档：参与研发会议；与产品经理、开发、测试人员沟通细节；设计文档结构。

- 文档发布与质保：帮助文档（Help）；快速指南（Quick Start Guides）；命令行文档（CLI）；版本注释 (Release Notes)；利用工具、技术审阅和同行互审保证文档质量。

- 撰写/审阅软件界面文字：与产品及研发讨论；设计撰写界面提示/描述/错误消息。

- 技术支持文档：撰写或更新面向用户或技术支持的解决方案（Salesforce/Odyssey）。

**工具**

- DocBook 与 Xmetal（开发基于 XML 格式的文档）

- Vasont（存储、管理、组织、发布文档）

- Acrolinx（文档质量检查工具）

- Jira（追踪研发需求、进度、验收标准）

- Confluence（用于收集写作资料）

- Salesforce/Odyssey（用于创作、更新、发布技术支持内容）

### 2014/07 – 2014/11

[外语教学与研究出版社](https://www.fltrp.com) 助理编辑

**项目**

维护在线阅读网站爱洋葱 http://www.iyangcong.com

**职责**

- 参与禾林（Harlequin）图书翻译项目，联系译员，制定翻译计划，根据外研社标准审阅译文，并在爱洋葱网站定期发布双语图书。

### 2013/08 – 2014/01

[北京瞰绩科技有限公司（comScore）](https://www.comscore.com) 实习技术翻译

**项目与职责**

- 使用计算机辅助翻译工具（Across）翻译 comScore 英文网站；翻译 comScore 产品 Digital Analytix 的软件界面，用户指南；白皮书。

- 协助销售总监处理办公室事务。

**工具**

- Across（类似 Trados 的桌面计算机辅助翻译软件）

### 2012/07 – 2013/03

[科多思科技有限公司（Sigma Kudos）](https://www.sigma.se/) 实习文档工程师

**项目**

- RBS（爱立信基站产品）

- CDMA

**职责**

- 协助开发爱立信产品软硬件文档：根据爱立信提供的材料维护爱立信文档；组织文档审阅。

- 文档发布：RBS产品的技术规格文档，硬件安装文档；CDMA 产品的命令行文档。

- 文档格式转换：将爱立信的历史文档从 MS Word 格式转换到基于 DITA 的 XML 文档。

**工具**

- Abortext Editor

- Adobe Illustrator

## 主要技能

- 中英文文档与用户界面写作

- 文档代码化/基于 XML 的技术写作（DITA/DocBook）

- 基本矢量图绘制（Adobe Illustrator/InkScape）

- 翻译/本地化

- 熟悉敏捷开发流程

## 外语水平

英语专业八级；人事部英语笔译二级翻译人员资格
