# 谋杀长句（界面版）

在目前的工作中，我常需要与维多利亚式的英译文较劲，十分头疼。

下面是一个真实的例子。

**例一**

>原文：未进行目标集群同名虚拟机检查。
>
>译文：The check in the target cluster to make sure that there are no VMs bearing the same VM names as in the source cluster has not been performed.

我们先介绍例一的背景。将虚拟机迁移到目标集群前，需要预先检查在目标集群中是否已存在同名虚拟机。如果目标集群中已存在同名虚拟机，则无法进行本次迁移。

我所谓的`维多利亚式`英译文，例一就是典型，这种译文往往有如下特点：

- 第一眼读过去大概率看不懂
- 常使用非技术英语用词，如上例中的 `bear` 在柯林斯词典中释义如下：

    **bear**：If something bears a particular mark or characteristic, it has that mark or characteristic. 带有 (标记或特征)
- 常使用长句，且句式冗杂、拖沓。

下面我们看看如何改进。

**例一**来自下面的错误消息 2

>错误消息 1：检查目标集群同名虚拟机时遇到问题  
>此时用户点击「下一步」  
>错误消息 2：未进行目标集群同名虚拟机检查

可见这两条错误消息会连续出现，而错误消息 2 仅用于提示用户「某个检查未完成」。

现在我们开始翻译迭代。

实验版本一：

>简化版：  
>VM name duplication check was not performed for the target cluster.

实验版本二：

>为避免歧义，可能需要在翻译中补充信息：  
>VM name duplication check was not performed between the source and target clusters.

实验版本三：

>鉴于有上下文，或许又不需要说得这么死板：  
>VM name duplication check was not performed.

可这样翻译就行了吗？读到这里，不知你是否已发现中文原文中潜藏的问题。其实核心问题并非在于「源集群与目标集群之间是否存在同名虚拟机」，而是「在目标集群中该虚拟机名称是否已被占用」。而中文原文在这一点上并不明确，甚至可以说从某种程度上会误导翻译，比如我在审校中碰到的**例二**。

>原文：虚拟机在目标集群存在名称相同的虚拟机。继续下一步将自动为其重命名。
>
>译文：In the target cluster, the following VMs bear the same VM names as in the source cluster. If you proceed to the next step, they will be automatically renamed.

到这里我发现，为了给出最合适的译文，首先需要建议修改原文。与对产品中文界面负责的设计同事沟通后，中文原文修改如下：

**例一**

>「未进行目标集群同名虚拟机检查。」  
> 改为  
>「未检查虚拟机名称是否已在目标集群中被占用」

**例二**

例二原中文文案写作时以「虚拟机」与「虚拟机名」之间的关系为主线，这样容易导致英文译文繁琐。修改后，以虚拟机名为主要话题，行文逻辑更简单，更便于翻译。

>「虚拟机在目标集群存在名称相同的虚拟机。继续下一步将自动为其重命名。」  
> 改为  
>「该虚拟机名称在目标集群已被占用。继续下一步将自动为其重命名。」 

在此基础上，可以建议译为：

**例一建议译文**

>VM name availability check was not performed.  
>或不妨补充信息为「因为某种原因，某个检查未完成」再翻译：  
>Unable to check VM name availability in the target cluster. 

**例二建议译文**

>The following VM names have been used in the target cluster. Proceed and these VMs will be automatically renamed.

就这样，在短短的两条界面文案翻译的审校工作中，我完成了与同组文档工程师同事以及设计同事的沟通协作。

**总结**

出现维多利亚式的译文，究其原因，应有如下两点：

- 译者很少阅读技术类中英文，缺乏平行文本输入。在翻译时受到过往阅读经验的影响，特别是语言、翻译背景的同学，译文通常带有文学色彩、或者有法律文书的风格，如例一、例二中的 `bearing the same VM names`。
- 译者已经脱离了 `word-for-word` 的翻译模式，能够做到在理解原文的基础上翻译，但囿于原文结构或背后复杂的隐含语义，为了消除歧义取了忠实、准确，但舍弃了简洁。

我相信多读技术类平行文本，在此基础上多了解自己所写作的产品，写作时多思考，一定能在技术写作中产出简洁明快、清晰易懂的英文。
