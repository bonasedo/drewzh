# Drew (Yinghao) ZHAO

**Technical Writer**

<details>
<summary>Contact me</summary>

- [LinkedIn Profile](https://www.linkedin.com/in/drew-zhao-b9162256/)
- <drewzhao@outlook.com>
- <details><summary>Wechat</summary><img src="resume-wechat.jpeg" width="256" height="256"></details>

</details>

## Introduction

I love to investigate product details, give feedback on usability, and write from a user’s perspective. I love clear, concise, and logical writing.

I feel proud in filling communication gaps. I believe being professional means being accountable. As a writer, I often find mismatch in inputs, and by discovering the truth or missing piece I can contribute to making a better product, and a better team.

## Education

2011/09 – 2014/07

Master of Engineering (focused on CAT, L10N and technical writing), Peking University

2007/09 – 2011/07

Bachelor of English, Hunan Agricultural University

## Experience

### 2021/07 - Present

[RongCloud](https://www.rongcloud.cn) Technical Writer

**Responsibilities**

- As the newly introduced writer role, I helped standardize the workflow for contributing content. I created rules and standards, and kept on introducing best practices from excellent documentation projects.
- Reviewed and contributed to the developer-facing documentation (in Chinese) for SDKs and web APIs.
- Responsible for the translation of documentation. Responsibilities include proofreading translations from translation service providers, editing translations, and editing the source content for better translation results.   
- Managed documentation projects for each release and coordinated content contributions from engineering or product management teams. 

**Tools Used**

- Markdown + VS Code

- Git/SourceTree/Gitlab

- OmegaT + Okapi filters (CAT tool)

### 2020/06 - 2021/07

[Terminus Group](https://www.terminusgroup.com/) Technical Writer

**Responsibilities**

- As the lone writer, I created the Chinese and English product guides, quick-start guides, user guides, installation guides for Terminus robots.
- Acted as a project communicator and facilitator, I helped with the project delivery of over 150 robots to Dubai Expo. For example, I attended the discussion of usage scenarios in Dubai Expo, documented the scenarios and verified each scenario along with field test engineers. 
- Helped the Robotics team to build an intranet SharePoint platform to facilitate collaboration.
- Interfaced with the company security team on the Data Privacy Impact Assesment（DPIA）of the robots products for Dubai Expo.
- Translated the UI strings on the robots and the management platforms.

**Tools used**

- Markdown + VS Code / DITA / DITA Open Toolkit, for organizing markdown files to generate PDF

- Git/SourceTree/Gitlab

- Adobe Illustrator / Adobe Photoshop / Inkscape / Gimp / ImageMagick

- OmegaT + Okapi filters (CAT tool)

- SharePoint

### 2018/10 – 2020/05

[SmartX](https://www.smartx.com/global) Technical Writer

**Content Authoring**


- Created from scratch or edited software installation and administration guides, CLI references, and scenario-based technical solutions, mainly in Chinese
- Translated product UI strings and website

**Technical Writing Tooling**

- Designed and completed the migration from Google Docs to a docs-as-code approach

- Designed and implemented a PDF creation solution based on the free open source DITA Open Toolkit, and integrated it with the docs-as-code workflow

**Tools Used**

- Google Docs

- Markdown + VS Code

- Git / Gitlab / Github / Gitlab CI

- Docusaurus, a static site generator opensourced by Facebook

- Inkscape, a free open source alternative to Adobe Illustrator

- Graphviz, an open source software for drawing diagrams

- DITA / DITA Open Toolkit, for organizing markdown files to generate PDF

- SmartCat, an online CAT tool

### 2018/8 – 2018/8

[Cheetah Mobile](https://www.cmcm.com/?hl=en) Localization Product Manger

**Responsibilities**

- Wrote or translated UI texts into English for mobile apps including CM Launcher, CM Keyboard, Bawls of Arrows, Bitrue, CM Security, and so on

- Worked with translation vendors (RWS Moravia, EC Innovations, OneHourTranslation) to translate English output to 20+ languages; monitored translation project statuses

- Committed translation to 10+ projects’ code (**SVN or Git**) for weekly releases after manually running diffs between old and new language resource files; mitigating priorities while **highly stressed**

- Translated product campaigns, internal mails, and so on

**Tools Used**

- Git/SVN/SourceTree

- BeyondCompare for diffing, and then selecting content to commit

- Crowdin, an online crowd sourcing CAT tool

### 2014/12 – 2018/7

[Veritas (Split from Symantec)](https://www.veritas.com) Information Developer

**Projects**

- NetBackup Appliance (Backup and restore)

- Velocity Appliance (Copy data management)

- NetBackup Virtual Appliance (Backup and restore in virtual
environment)

- Access Appliance (Long term retention of data)

**Responsibilities**

- Software documentation: **met with/interviewed** product owners, UX designers, developers, and testers for **key concepts, procedures, and bug details**; designed the information architecture; incorporated review comments

- Error message and UI strings refining: **standardized the doc process for error handling**; edited the message strings and solutions for Unified Message Identifier (UMI) that enables customers to solve problems on their own

- Content delivery and assurance: delivered content in Help, Quick Start Guide (QSG), Man pages, Release Notes, and so on; provided on-demand document quality assurance and publishing service to global Information Development teams

- Technical Support documentation: **acted as a key communicator** in investigating bug details with developers and testers; documented or reviewed public/internal solutions to software known issues

**Tools used**

- **DocBook** and **Xmetal** for authoring XML-based content

- **Vasont** for storing/organizing/publishing content

- **Acrolinx** for ensuring content quality

- **Jira** for tracking requirements/progress/acceptance criteria and so on

- **Confluence** for sourcing input from wiki

- **Salesforce**/**Odyssey** for authoring/publishing technical support content

### 2014/07 – 2014/11

Foreign Language Teaching and Research Press Associate Editor

**Projects**

- Launching and maintaining content for the online reading website: http://www.iyangcong.com

**Responsibilities**

- Planned and managed book translation projects: the Harlequin project that was to produce bilingual books to Chinese college students

- **Proofread translations** from freelance translators according to FLTRP standards

- Produced **Markdown** files for publishing the content to the iyangcong website

### 2013/08 – 2014/01 (Intership)

[comScore](https://www.comscore.com) Technical Translator Intern**

**Projects**

- Digital Analytix, a web analytics product (sold to Adobe in 2015)

**Responsibilities**

- **Translated comScore web analytics software** Digital Analytix, covering the software GUI, user guides, white papers, and technical procedures such as how to implement the comScore products in websites and apps. **I chose** the CAT (computer-aided translation) tool "Across" for this project. **Based on this project, I wrote my master thesis.**

- Helped the VP of sales with office chores

**Tools used**

- Across, a CAT Tool

### 2012/07 – 2013/03 (Intership)

[Sigma Kudos]((https://www.sigma.se/)) Technical Writer Intern

**Projects**

- RBS

- CDMA

**Responsibilities**

- Software and hardware documentation: worked with **Ericsson** developers on-site or remotely to gather input; **read and analyzed R\&D input materials to extract information for writing**; developed XML-based content with the Ericsson customized **Abortext** and **Adobe Illustrator**; scheduled reviews with stakeholders and incorporated comments.

- Documentation delivery: Technical Specification, Hardware Installation, Hardware Replacement, etc. for Ericsson RBS products and man pages for CDMA software.

- Document conversion: converted technical documents from MS Word docs into XML-based **DITA** files.

**Tools used**

- Abortext Editor

- Adobe Illustrator

## Skills

- Docs-as-code approach: managing content as code and publishing content to pdf or doc websites

- Authoring xml-based structured content in **DocBook** or **DITA**

- Developing information in an **Agile** environment for 3+ years

- Technical illustration skills with MS Visio/InkScape

- Certificate: China Accreditation Test for Translators and Interpreters (CATTI) – Translator Level 2.

## Languages

- Mandarin: Native

- English: Work proficiency. Passed National English Test for English Major – Band 8.
