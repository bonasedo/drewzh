# Chinese fonts and DITA OT

> using the free open source Apache Fop!

TL;DR: Start from [How to embed custom Chinese fonts in PDF](#how-to-embed-custom-chinese-fonts-in-pdf).

## Why this article

Currently DITA Open Toolkit has released 3.4, but most content (books, blogs, forum discussions) I've found on the Internet about customizing DITA-OT is obsolete, especially in Chinese technical communication context. That's why I wrote this article in the first place.

********

## Why I need to embed custom Chinese fonts in PDF

Thanks to the open source world, we can use the free open source [DITA Open Toolkit](https://www.dita-ot.org/) and the embedded fo-processor Apache Fop (also open source and free!) to output PDF for out docs-as-code documentation. However, lots of our content is in Simplified Chinese, so enhancing the tool's support for Chinese fonts is crucial for us.

By default, DITA Open Toolkit enables a font mapping mechanism for dealing with languages that may require separate [glyphs](https://en.wikipedia.org/wiki/Glyph) than western languages. Basically, it maps "logical fonts" such as `sans-serif`, `serif` to "physical fonts" (actual fonts that should be used) like `Helvetica`, or `Time New Roman`, for example. This mapping allows us to only use `logical` fonts when defining <font-family> attributes that affect the looks of PDF in the xml stylesheets(xslt). And there's more to it but it's out of the scope of this article.

>Currently the use of the font mapping mechanism is for backward compatibility and special use cases, and you can disable it once and for all. However, in that case you'll need to go to every xsl where a <font-family> references logical fonts, and replace them with physical fonts. Read Jarno Elovirta's [Font configuration in PDF2](https://www.elovirta.com/2016/02/18/font-configuration-in-pdf2.html) for more information.

The tools comes with a default mapping definition (shown below) in its `fop.xconf` file found at `*YOUR DITA OT FOLDER*/plugins/org.dita.pdf2/cfg/fo/font-mappings.xml`:

```
<logical-font name="Sans">
      <physical-font char-set="default">
        <font-face>Helvetica, Arial Unicode MS, Tahoma, Batang, SimSun</font-face>
      </physical-font>
      <physical-font char-set="Simplified Chinese">
        <font-face>AdobeSongStd-Light, Arial Unicode MS, Batang, SimSun</font-face>
      </physical-font>
      <physical-font char-set="Japanese">
        <font-face>KozMinProVI-Regular, Arial Unicode MS, Batang</font-face>
      <physical-font/>
...
<logical-font/>
```        

You can find in `font-mappings.xml` that the `default` and `Chinese` char-set groups include `Arial Unicode MS`. In fact, it's the only font that supports Chinese characters in these two groups. However, "[the font was included with Office but not Windows](https://docs.microsoft.com/en-us/typography/font-list/arial-unicode-ms)". That is to say, this font does not come installed with any operation system, and if the default `font-mappings.xml` is left unchanged, anyone viewing your PDF from an OS where the font is absent would only see continuous hashtags(#) where Chinese characters should've appeared.

An expedient solution would be to substitute this Arial Unicode MS with something that supports Chinese(in my case) and readily available on all OS platforms. But I would prefer an alternative, to just embed Chinese fonts to our liking in PDF, and avoid the fuss to test font availability on OSes. This approach increases the PDF file size (the PDF carries all glyphs it uses, usually a subset of what the font provides), but it solves the problem of relying on fonts available on the viewer's OS.

*********

## How to embed custom Chinese fonts in PDF

Because we need substantial customizations on the default PDF output, we follow the best practice to use a custom plugin.

>This artiles focuses on how to use custom Simplified Chinese fonts with a plugin, and basically the customization applies to any method of [PDF customization approaches](https://www.dita-ot.org/3.4/topics/pdf-customization-approaches.html).

This procedure is based on the example plugin (it's a folder named `com.example.print-pdf`) that comes with the DITA-OT package. After unzip, the example is found at `*YOUR DITA OT FOLDER*/docssrc/samples/plugins/com.example.print-pdf`.

1. Copy `font-mappings.xml` from `*YOUR DITA OT FOLDER*/plugins/org.dita.pdf2/cfg/fo/` to `*YOUR DITA OT FOLDER*/plugins/com.example.print-pdf/cfg/fo/`.

2. Substitue the `Arial Unicode MS` with the custom Chinese fonts to your liking in `font-mappings.xml`. Make sure you reference by the fonts by postscript names.

    I use the free [Source Han Sans](https://github.com/adobe-fonts/source-han-sans) and [Source Han Serif](https://github.com/adobe-fonts/source-han-serif) fonts. Thanks to the open source world again!

    Here's a few lines from my `font-mappings.xml` file.

    ```
    <logical-font name="Sans">
          <physical-font char-set="default">
            <font-face>Helvetica, Tahoma, Batang, Source Han Sans CN</font-face>
          </physical-font>
          <physical-font char-set="Simplified Chinese">
            <font-face>AdobeSongStd-Light, Batang, Source Han Sans CN</font-face>
          <physical-font/>
    ...
    <logical-font/>
    ```

    Now you've let DITA-OT know what physical fonts you want to use and embed in the PDF output.

3. To integrate the customized `font-mappings.xml` to the example plugin, add the following line to the `catalog.xml` file in `com.example.print-pdf/cfg/`:

    ```
    <uri name="cfg:fo/font-mappings.xml" uri="fo/font-mappings.xml"/>
    ```

    >**Note**: If you happen to have read Leigh W. White's book "[Dita for Print: A DITA Open Toolkit Workbook, Second Edition](https://www.goodreads.com/book/show/34416619-dita-for-print)", **DO NOT ADD** `<property name="pdf2.i18n.skip" value="false"/>` **TO** `integrator.xml`. As many times I've tested, adding this line, be the value set to `false` or `true`, causes Fop to output `#` placeholders for Chinese characters.

    >In the next three steps, you'll provide the font files to Fop. (DITA-OT converts the source to xsl-fo files, and deliver them to Fop, the one that works behind to converts the mess you created to PDF).

4. Copy the FOP configuration file, `fop.xconf`, which lives in the plugin path `org.dita.pdf2.fop/cfg`, to the counterpart path in the example plugin, which is `com.example.print-pdf/cfg/`.

    >Creating a separate `fop.xconf` would help contain all the customizations in the plugin, and leave the original code untouched, which is a recommended best practice, especially in terms of reusing custom plugins across platforms and DITA-OT versions.

    Later we'll use the `args.fo.userconfig` property with the `dita` command to specify this user-configured `fop.xconf`.

5. Copy your custom Chinese font files (you may want to use .otf or .ttf files) to the same directory of the `fop.xconf`, which is `com.example.print-pdf/cfg/`.

6. Look for the `<renderer mime="application/pdf">` in `fop.xonf`, and add the font registration information inside the `<fonts>` tags. By this step, Fop knows how to find the fonts it'll embed in the PDF output.

    > You may want to read through [Register Fonts with FOP](https://xmlgraphics.apache.org/fop/2.4/fonts.html#register) from Apache Fop documentation carefully before you've an idea of how to set all the attributes.

    Here's how I register the `Source Han Sans CN` and `SourceHan Serif CN` fonts.

    ```
    <fonts>
        <font kerning="yes" embed-url="SourceHanSansCN-Normal.otf" embedding-mode="subset">
               <font-triplet name="Source Han Sans CN" style="normal" weight="normal"/>
        </font>
        <font kerning="yes" embed-url="SourceHanSansCN-Bold.otf" embedding-mode="subset">
               <font-triplet name="Source Han Sans CN" style="normal" weight="bold"/>
        </font>
        <font kerning="yes" embed-url="SourceHanSerifCN-Regular.otf" embedding-mode="subset">
               <font-triplet name="Source Han Serif CN" style="normal" weight="normal"/>
        </font>
        <font kerning="yes" embed-url="SourceHanSerifCN-Bold.otf" embedding-mode="subset">
               <font-triplet name="Source Han Serif CN" style="normal" weight="bold"/>
        </font>

    <!-- auto-detect fonts -->
    <!--<auto-detect/>-->

    </fonts>
    ```

7. You're almost done. Run the following command to install the customized example plugin to DITA-OT:

    ```
    dita --install
    ```

8. Finally, verify your customizations by generating an output:

    ```
    dita --args.fo.userconfig="your-customized-fop.xconf-path/fop.xconf" -i your-ditamap.ditamap -f print-pdf
    ```

    Where `print-pdf` is the transtype added by the example plugin.

Check the PDF document property in Adobe Reader, and you can find the embedded fonts listed under the font tab.

## Conclusion

DITA Open Toolkit is a very powerful publishing tool used to process DITA content (in our case we use it to process markdown) and convert it to other formats. It's also the tool behind many well-known powerful commercial tools such as Adobe FrameMaker, oXygen XML Editor, XMetaL Author Enterprise, just to name a few. It's especially valuable to us in that it allows us to single source from markdown content in combination with some readily available DITA features.

**Key points**:

1. DITA Open Toolkit can be enhanced to support custom Chinese fonts to your liking.
2. The font mapping mechanism is not something you should desperately avoid, as put by [Jarno Elovirta](http://elovirta.com/), "the font mapping process has its use cases and it's a working solution" for Fop's lack of support for the font-selection-strategy property. In my case, it saves me the trouble to modify multiple xml files, and makes my plugin more adaptable to further customizations.
3. If you've decided not to use font mapping, you can just edit the `*YOUR DITA OT FOLDER*/config/configuration.properties` file, change the value of `org.dita.pdf2.i18n.enabled` to `false`. Note that this is a global change that affects all plugins that depends on the PDF2 plugin. And then you can search under your custom plugin folder's `fo` directory for the font names you intend to replace.

## Promoting DITA Open Toolkit

We've built a docs-as-code workflow and a free toolchain, and DITA Open Toolkit is a key part of it. The prevalence of the `docs-as-code` or `docs-like-code` approach has undermined the importance of PDF as an output format, which may be also in part due to its lack of straightforward, readymade solutions (push-button experience). Tom Johnson has listed PDF as one of the "more challenging factors" in his [docs as code tutorial on I'd rather be writing](https://idratherbewriting.com/learnapidoc/pubapis_docs_as_code.html#dealing-with-more-challenging-factors). Indeed, when PDF is a requirement, various home-made solutions have flourished. Some are CSS-based tools like [weasyprint](https://weasyprint.org), [wkhtmltopdf](https://wkhtmltopdf.org), [Prince](https://www.princexml.com), and some SSGs' native ability to output PDF rely on these tools. [LaTeX](https://www.latex-project.org/) is also an option. I've also seen PDF converted by MS Word from docx files that are automatically assembled from structured content. DITA Open Toolkit is also an option that you may want to experiment with. Overall, the docs-as-code approach is an alternative to DITA, DocBook, or other well-established standards; not something contradictory or a simple replacement. Try DITA-OT and see whether your (to-be) established docs-as-code workflow can benefit from the DITA world.

## Comments

- [Dita for Print: A DITA Open Toolkit Workbook, Second Edition](https://www.goodreads.com/book/show/34416619-dita-for-print) by Leigh W. White is a great book though it was based on DITA-OT 2.x. But it help you figure out the basics of how to customize the stylesheets.
-  [PDF Plugin Generator](http://dita-generator.elovirta.com/) never worked for me despite it says version 3.0 is supported.
- About this topic here are two relevant references in Chinese. But as I've said, most are not up-to-date, and not tested and verified with the current DITA-OT version.
    - [DITA-OT之自定义PDF输出](https://www.jianshu.com/p/216f8d5f8531)
    - [DITA Open Toolkit 发布PDF时中文乱码的解决方法](http://blog.sina.cn/dpool/blog/s/blog_5f4150730102vo0r.html)