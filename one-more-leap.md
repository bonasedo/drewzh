# One More Leap to Revitalizing Your Digital Assets

Back in 2018, IDC has predicted [the total amount of data will grow from 33ZB to a 175ZB by 2025.](https://www.seagate.com/files/www-content/our-story/trends/files/idc-seagate-dataage-whitepaper.pdf) This year [an updated IDC report](https://www.idc.com/getdoc.jsp?containerId=prUS46286020) shows that 59 TB of data will be created, captured, copied and consumed alone in 2020. For many of us, these are just tectonic figures. For CXOs and IT managers, digital transformation is so tangible as these statistics depict the mammoth of digital assets, which could easily become a liability.

## Understanding Digital Transformation

Wikipedia’s defines digital transformation as the use of new, fast and frequently changing digital technology to solve problems, which implies that digital transformation is a never-ending process. We cannot stop new technologies and solutions from emerging. As we embrace these technologies such as blockchain, 5G, Internet of things, AI, we see efficiency boosted and productivity multiplied in great leaps, at the same time the accumulation of data is nothing better described than an avalanche.

Over the years, CXOs and IT managers from big and small alike are busy day-to-day delivering business values while having to now and then evaluate their current technology stack and then making responsible and informed decisions. They need (so badly) ease with data management, visibility across the whole organization, more insightful outcomes from their digital assets, better ROIs, and of course, more resources. Unfortunately, this sounds like an unrequited request. Why? The answer is apparent.

Complexities get in the way.

## Unstructured Data

Unstructured data is the biggest elephant in the room. Verses structured data like relational databases or semi-structured data such as HTML or XML files, unstructured data like videos, audios and images aren’t something easy to derive values from via data analytics, at least not until AI technologies have matured in the recent years. According to Paul Nelson, Innovation lead of Accenture Applied Intelligence, “[Most organizations are making good use of structured data (tables, spreadsheets, etc.), but a lot of untapped business-critical insights are held in unstructured data](https://www.accenture.com/us-en/blogs/search-and-content-analytics-blog/search-unstructured-data-analytics-trends)”, and “[organizations are waking up to the fact that 80% of their content is unstructured.](https://www.accenture.com/us-en/blogs/search-and-content-analytics-blog/search-unstructured-data-analytics-trends)”

However, unstructured data is growing each year at a mind-boggling speed. IDC predicts out that “80% of data will be video or video-like” in the report [*Managing Unstructured Data Requires a Fresh Approach*](https://www.quantum.com/globalassets/documents/idc-vendor-spotlight.pdf). This may be attributed to the growth of surveillance videos, or the popularity of video social networking (TikTok) as I understand. At the same time, enterprises are generally retaining more data than did in the past just to feed the AI-powered data mining and analytics in hope of some hidden values being uncovered.

All in all, organizations of all sizes have accumulated a daunting amount of data, held on to it while spending a lot storing and managing these assets. Yet they have not fully tapped into the tremendous gold mine. Data may reside anywhere/everywhere at all kinds of on-premise storage systems or on the cloud(s) depending on enterprises’ strategies.

Are organizations truly prepared for the data needs of the future?

## Just Authentic Data Needs

I’ve collected a few needs on exploiting untapped data assets (I’d call them sleeping asset beauties) across insurance, entertainment, and retailing industries. call these untapped data assets sleep beauties. Take a look.

- Insurance companies have a lot of user identity files, hand-written clinical records, photos of car accident scenes, etc. and would like to fully automate the process of validating insurance claims.

- Entertainment companies have copies of movies, TV series, and documentaries, etc. and would like to atomically generate highlights from these sources.

- Retailers and fast fashion companies have an abundance of user buying records, sales promotion data, legacy posters, user posts, etc. and would like to automatically create posters based on the prediction of the next season.

## Tapping into Your Digital Assets

AI has undoubtedly unleashed the power of unstructured data.

### Do It Yourself

Make no mistake. If you have excellent control of your data, leveraging or training existing resources to develop your own solution is the best way towards effective data governance. Leveraging your internal resources means you already have a very deep understanding of your data and based upon that you can develop tailored solutions that address the most detailed business requirements. It’ll also be relatively easy to migrate your existing solutions because your team have made and thus known every bit of the existing solution and the new business requirement, compared to adopting third-party solutions that may require a substantial amount of communication and training on the vendor’s implementation team’s side.

This solution may work best for either small teams that have a relatively small amount of data, or big tech giants that have very strong competitive edge on both talents and budgets. You may also want to look at open-source solutions provided in which you are fully capable of maintaining your own implementation and passionate to give back to the community.

The downside of this solution is that the development takes up some resources, and for a lack of experience, you may encounter problems. It’s basically more trial and error versus more certainty when compared to the following two solutions.

### RPA+AI

Commercial applications of robotic process automation (RPA) are not rarely seen nowadays. Many excellent vendors have emerged such as UiPath, Automation Anywhere and Blue Prism. By automating repetitive processes and tasks, RPA can greatly save costs in human labors and reduce human errors. For example, [Gatner says RPA can save finance departments 25,000 hours of avoidable work annually](https://www.gartner.com/en/newsroom/press-releases/2019-10-02-gartner-says-robotic-process-automation-can-save-fina#:~:text=Gartner%20studied%20the%20use%20of%20RPA%20in%20finance,key%20roadblocks%20that%20are%20currently%20hindering%20broader%20adoption.) in 2019.

AI technologies has greatly empowered the RPA industry. Technologies such natural language processing (NLP), Patten recognition algorithms, and speech-to-text conversion enable RPA to understand documents, images, audio and videos, and to extract meanings from the contents and then output structured data, making it possible for data scientists to easily extract business values from diverse data sources and across silos.

The advantages of RPA are obvious. Keep in mind:

- RPA works wonders in boosting efficiency if your organization have a large amount of simple and repetitive tasks in a highly digitalized environment. Note that for quite many tasks, you need to use an RPA-assisted human approach.

- RPA implementation takes time, at least 2-4 weeks depending on how familiar the implementation team is with your businesses. You will design your RPA processes and maintain them.

- Top RPA vendors may show better stability and compatibility when RPA processes work in an environment of heterogenous systems. Also, make sure to research your local market for RPA maturity before you choose a vendor.

### AWS WonderLake

It’ll be my remiss if there’s not a cloud solution. “[*Cloud Adoption in 2020*](https://www.oreilly.com/radar/cloud-adoption-in-2020/)”, a survey by O’Reilly shows that close to 40% of all respondents expect that all of their applications will run in a cloud context in the next 36 months. Public-cloud solutions have overwhelming advantages in terms of saving cost-efficiency and ease of use.

AWS WonderLake is an innovative cloud-based one-stop solution in this article’s context. It’s designed to help enterprises organize, manage and revitalize digital assets by leveraging a portfolio of highly automated AWS services that empower data consumers to extract insights from structured, semi-structured and unstructured data with ease. We’ll look at AWS WonderLake tackles the sleeping assets problem.

#### Security

Organizations struggle to have a panoptic visibility into their digital assets due to the complexities brought by years of on-going digital transformation. This fact makes security and compliance an outstanding problem. Not to mention these sensitive data may hide in hand-written documents, images, audios and videos. AWS WonderLake is able to use machine learning and pattern matching to cost efficiently analyze your entire data collection to help you locate privacy and compliance risks. Currently it supports Health Insurance Portability and Accountability Act (HIPAA) and General Data Privacy Regulation (GDPR).

#### Simple and Easy

Compared with the DIY approach, or RPA+AI way, AWS WonderLake might be the easiest solution in terms of implementation and usage. On the one hand, it’s a portfolio of fully managed and highly automated services, which mean the learning curve have been flatten quite a bit. On the other hand, the pressure on resources on IT infrastructures and talent acquisition is eliminated.

#### Customizable

For every service there’s an extensive list of options and parameters that provide refined control of the processes and the final results.

To sum up, AWS WonderLake is built on AWS’s decades of experience with data. The survey “[*Cloud Adoption in 2020*](https://www.oreilly.com/radar/cloud-adoption-in-2020/)” also shows that AWS accounts for 75% of usage, compared with 52% for Azure and 34% for GCP among the survey respondents. If you are going the cloud-y way, this solution deserves careful consideration.

## Conclusion

Revitalizing digital assets has been one action item for many organizations as the value of data has already been illuminated by AI technologies such as machine learning and deep learning. Blockchain also enable data to be transacted and consumed while ensuring its security. What’s to added to this big picture is unstructured data. In your journey of digital transformation towards the future of uncertainties, disruptions, challenges and opportunities, you just need to take one more leap.